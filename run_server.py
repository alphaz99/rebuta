# Note: this file is only used for testing locally. The production environment
# uses wsgi to start up, and bypasses this file. So we are free to have debug
# settings enabled.
import argparse

import rebuta
from rebuta import app

parser = argparse.ArgumentParser(description="Run development server.")
parser.add_argument("-e", "--env", default="dev")
parser.add_argument("-p", "--port", metavar="port", type=int, default=5000,
                    help="Port on which to run server.")


if __name__ == "__main__":
    args = parser.parse_args()
    rebuta.init(args.env)
    app.run(port=args.port)

