"""Default website configurations, used only for testing.
"""

from rebuta import environment

# Public Test Database
TEST = environment.Environment(
    db_hostname="localhost",
    db_name="rebuta_test",
    db_user="rebuta_test",
    db_password="public",
    debug=True,
    testing=True,
    secret_key="1234567890")
