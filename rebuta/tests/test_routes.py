import flask

from rebuta.tests.fixtures import client
from rebuta import app

def test_home(client):
  rv = client.get(flask.url_for('home'))

  assert rv.status_code == 200
