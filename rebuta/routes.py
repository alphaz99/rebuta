import flask
from rebuta import app

@app.route('/')
def home():
  return flask.render_template('rebuta.html')

@app.route('/contact')
def contact():
    return flask.render_template('contact.html')
